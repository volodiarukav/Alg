﻿#include <iostream>

void selection(int mas[], int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        int minznach = i;
        for (int j = i + 1; j < n; j++)
        {
            if (mas[j] < mas[minznach])
            {
                minznach = j;
            }
        }
        if (minznach != i)
        {
            int tmp;
            tmp = mas[i];
            mas[i] = mas[minznach];
            mas[minznach] = tmp;
         
        }
    }
}

int main()
{
    int mas[10];
    int n = 10;
    for (int i = 0; i < n; i++) {
        mas[i] = rand() % 10;
        std::cout << mas[i] << " ";
    }

    std::cout << std::endl;

    selection(mas, n);

    for (int i = 0; i < n; i++) {
        std::cout << mas[i] << " ";
    }
}